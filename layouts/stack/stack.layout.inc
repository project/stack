name = Stack
description = Default layout for DrupalVietnam.org.
preview = preview.png
template = stack-layout

; Regions
regions[top]                  = Top bar
regions[navigation]           = Navigation bar
regions[highlighted]          = Highlighted
regions[help]                 = Help
regions[content]              = Content
regions[sidebar_first]        = First sidebar
regions[sidebar_second]       = Second sidebar
regions[test]                 = Testimonial‎
regions[footer_firstcolumn]   = Footer first column
regions[footer_secondcolumn]  = Footer second column
regions[footer_thirdcolumn]   = Footer third column
regions[footer_fourthcolumn]  = Footer fourth column
regions[footer]               = Footer

; Stylesheets
stylesheets[all][] = css/layouts/stack/stack-layout.css
stylesheets[all][] = css/layouts/stack/stack-layout.no-query.css
